const each=require('./each.js');

function map(elements, cb) {
  try {
    const result = [];
    elements.forEach((element, index) => {
      result.push(cb(element, index));
    });
    return result;
  } catch (error) {
    console.error("Error occurred:", error.message);
  }
}

module.exports = map;

