const each=require('./forEach.js');

function flatten(elements) {

  try{
    const flattened = [];
    each(elements, element => {
        if (Array.isArray(element)) {
            flattened.push(...flatten(element));
        } else {
            flattened.push(element);
        }
    });
    return flattened;
   }catch(error){
    console.error("Error occurred:", error.message);
   } 
}


module.exports=flatten;