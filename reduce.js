function reduce(elements, cb, startingValue) {
  try {
    let accumulator = startingValue !== undefined ? startingValue : elements[0];
    const startIndex = startingValue !== undefined ? 0 : 1;

    for (let i = startIndex; i < elements.length; i++) {
      accumulator = cb(accumulator, elements[i]);
    }

    return accumulator;
  } catch (errror) {
    console.error("Error occurred:", error.message);
  }
}

module.exports = reduce;
