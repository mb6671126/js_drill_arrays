const filter = require ('../filter.js');

const items = require ('../datasetForArrayHOF.js');          

try{
const filtered = filter(items, element => element > 3);
console.log(filtered);

}catch(error){
    console.error("Error occurred while testing 'filter' function:", error.message);   
}