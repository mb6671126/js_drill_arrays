const reduce = require ('../reduce.js');

const items = require ('../datasetForArrayHOF.js');

try{
const sum = reduce(items, (accumulator, currentValue) => accumulator + currentValue);
console.log(sum);

}catch(error){
    console.error("Error occurred while testing 'reduce' function:" ,error.message);
}