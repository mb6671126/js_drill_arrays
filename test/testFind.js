const find = require ('../find.js');

const items = require ('../datasetForArrayHOF.js');

try{

const found = find(items, element => element === 5);
console.log(found);

}catch(error){
    console.error("Error occurred while testing 'find' function:", error.message);
}